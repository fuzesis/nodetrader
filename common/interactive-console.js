/**
 * New node file to test the interactive module as a console app
 */
'use strict'

var interactive = require('./interactive');

var looper = interactive.looper();
var questioner = interactive.questioner();

looper.clearOptions()
.addOption('display option', function() {
	console.log('display crap');
	looper.listen();
})
.addOption('inner loop option', function() {
	innerLoop(looper.listen);
})
.addOption('random test', optRandom)
.addOption('music option', function() {
	questioner.clearQuestions()
	.addQuestion('fav band', 'the saints')
	.addQuestion('fav song', 'know your product')
	.askQuestions(function(answers) {
		answers.forEach(function(elt, i) {
			console.log(elt.msg + ' is ' + elt.val);
		});
		looper.listen();
	});	
})
.addOption('random dates', function() {
	questioner.clearQuestions()
	.addQuestion('pls enter\n...year', '2014')
	.addQuestion('...num of occurances in year', '100')
	.askQuestions(function(answers) {
		var yr = parseInt(questioner.getAnswer(answers, 0));
		var nof = parseInt(questioner.getAnswer(answers, 1))
		optDatesInYear(yr, nof);
		looper.listen();
	});	
})
.addOption('this will be ignored as there is no callback')
.addOption('some xml shite', optionToTestXML).listen();

function optionToTestXML() {
	questioner.clearQuestions()
	.addQuestion('Pls enter\n...first name')
	.addQuestion('...middle name')
	.addQuestion('...surname')
	.askQuestions(function(answers) {
		// create some XML
		var xmlArr = [];
		answers.forEach(function(elt, i) {
			switch (i) {
			case 0:
				xmlArr.push('<firstName>' + elt.val + '</firstName>');
				break;
			case 1:
				xmlArr.push('<middleName>' + elt.val + '</middleName>');
				break;
			case 2:
				xmlArr.push('<surname>' + elt.val + '</surname>');
				break;
			}
		});
		console.log('<person>');
		xmlArr.forEach(function(elt, i) {
			console.log(elt);
		});
		console.log('</person>');
		looper.listen();
	});	
}

function innerLoop(exit) {
	var looper = interactive.looper();
	looper.setExit(exit)
	.addOption('inner option', function() {
		questioner.clearQuestions()
		.addQuestion('type anything')
		.addQuestion('and again')
		.askQuestions(function(answers) {
			console.log('first is ' + questioner.getAnswer(answers, 0));
			console.log('second is ' + questioner.getAnswer(answers, 1));
			looper.listen();
		});
	})
	.addOption('another inner option', function() {
		questioner.clearQuestions()
		.addQuestion('type some more anything')
		.addQuestion('and some')
		.askQuestions(function(answers) {
			console.log('first is ' + questioner.getAnswer(answers, 0));
			console.log('second is ' + questioner.getAnswer(answers, 1));
			looper.listen();
		});
	})
	.listen();
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

var appConfig = {
"categories": [
	"jumper",
	"trousers",
	"shorts",
	"socks",
	"t-shirt",
	"tie",
	"shirt",
	"other"
	],
"statuses": [
	"lost",
	"found",
	"matched",
	"archived"
	]
};

function optRandom() {
	var howMany = [];
	var max = appConfig.categories.length;
	for(var i=0; i<max; ++i) {
		howMany[i] = 0;
	}
	for(var i=0; i<1000; ++i) {
		var ix = getRandomInt(0, max);
		if (appConfig.categories[ix]) {
			howMany[ix] = ++(howMany[ix]);
		}
	}
	console.log('each category appeared...');
	appConfig.categories.forEach(function(elt, i) {
		console.log(elt + ' appeared [' + howMany[i] + ']');
	});
	looper.listen();
}

function optDatesInYear(yr, nof) {
	if (!isNaN(yr) && !isNaN(nof)) {
		var days = [];
		for(var i=0; i<365; ++i) {
			days[i] = 0;
		}
		for(var i=0; i<nof; ++i) {
			var ix = getRandomInt(0, 365);
			days[ix] = ++(days[ix]);
		}
		days.forEach(function(elt, i) {
			if (elt > 0) {
				var dt = new Date(yr, 0, i + 1);
				console.log(dt.toDateString() + " has [" + elt + "] activities");
			}
		});
	}
}
