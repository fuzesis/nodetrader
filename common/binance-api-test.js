/**
 * Node file to test the binance API via a console app
 */
'use strict'

const https = require("https");
//const { URL } = require("url");
//const axios = require('axios');
const interactive = require('./interactive');

const looper = interactive.looper();
const questioner = interactive.questioner();

const binanceExchange = new BinanceExchange();
const binanceExchangeAPI = new BinanceExchangeAPI();

looper.clearOptions()
.addOption('load binance exchange info', function() {
  console.log('loading binance exchange info...');
  binanceExchangeAPI.loadExchangeInfo(
    (data) => {
      binanceExchange.setExchangeInfo(data);
      console.log("...done");
      looper.listen();
    },(err) => {
      console.log("Error: " + err.message);
      looper.listen();
    });
})
.addOption('display binance rate limits', function() {
  const rls = binanceExchange.getExchangeRateLimits();
	console.log(JSON.stringify(rls));
	looper.listen();
})
.addOption('display all binance symbols', function() {
  const ss = binanceExchange.getExchangeSymbols();
  if (ss) {
    console.log('# of symbols: ' + ss.length)
    ss.forEach(s => console.log(JSON.stringify(s['symbol'])));
  }
	looper.listen();
})
.addOption('add some symbols you want to follow', function() {
  // always try to add these
  binanceExchange.addSymbol('poo');
  binanceExchange.addSymbol('XRPBTC');
  binanceExchange.addSymbol('XRPBTC');
  binanceExchange.addSymbol('XRPUSDT');
  binanceExchange.addSymbol('XRPTUSD');
  binanceExchange.addSymbol('XLMBTC');
  // now you add
  questioner.clearQuestions()
  .addQuestion('pls enter a valid exchange symbol', 'nowt')
	.askQuestions(function(answers) {
		binanceExchange.addSymbol(questioner.getAnswer(answers, 0));
		looper.listen();
	});	
})
.addOption('display followed symbols', function() {
  const ss = binanceExchange.getSymbols();
  console.log(ss.length);
  ss.forEach((s) => console.log(s));
	looper.listen();
})
.addOption('load ticker prices for followed symbols', function() {
  const ss = binanceExchange.getSymbols();
  console.log('# of symbols: ' + ss.length)
  if (ss.length < 1) {
    looper.listen();
    return;
  }
  ss.forEach((s, ix) => {
    binanceExchangeAPI.loadTickerPrice(
      s, (data) => {
        binanceExchange.addTickerPrice(data)
      }, (err) => {
        binanceExchange.addTickerPrice({ symbol: s, error: err.message });
      });
  });
  looper.listen();
})
.addOption('display followed symbol ticker prices', function() {
  const tps = binanceExchange.getTickerPrices();
  console.log(tps.length);
  tps.forEach((tp) => {
    console.log(JSON.stringify(tp));
  })
	looper.listen();
})
.listen();

/**
 * Performs a binary search on the provided sorted list and returns
 * the index of the item if found. If it can't be found it'll return -1.
 *
 * @param {*[]} list Items to search through.
 * @param {*} item The item to look for.
 * @param {*} listItem The function that returns the list item to compare
 * @return {Number} The index of the item if found, -1 if not.
 */
function binarySearch(list, item, listItem) {
  var min = 0;
  var max = list.length - 1;
  var guess;

  listItem = (listItem)? listItem:(ix) =>{
    return list[ix];
  };

  while (min <= max) {
      guess = Math.floor((min + max) / 2);

      const theItem = listItem(guess);

      if (theItem === item) {
          return guess;
      }
      else {
          if (theItem < item) {
              min = guess + 1;
          }
          else {
              max = guess - 1;
          }
      }
  }

  return -1;
}

/**
 * Contains the data related to the Binance exchange.
 */
function BinanceExchange() {
  if (!(this instanceof BinanceExchange)) {
    return new BinanceExchange();
  }
  var _exchangeInfo = null;
  var _symbols = [];

  function setExchangeInfo(exchangeInfo) {
    // check valid
    _exchangeInfo = (exchangeInfo)? exchangeInfo:null;
    if (!(_exchangeInfo)) {
      return;
    }
    // sort the 'live' array for future binary search
    var ss = _getExchangeInfoParts('symbols');
    ss.sort((a, b) => {
      if (a.symbol < b.symbol) {
        return -1;
      }
      if (a.symbol > b.symbol) {
        return 1;
      }
      return 0;    
    });
  }

  function _getExchangeInfoParts(validPart) {
    if (_exchangeInfo) {
      switch (validPart) {
        case 'rateLimits':
          return _exchangeInfo['rateLimits'];
        case 'symbols':
          return _exchangeInfo['symbols'];
      }
    }
    return null;
  }

  function _addSymbol(symbol) {
    // validate against exchange symbols
    if (symbol && _exchangeInfo) {
      const symObjs = _getExchangeInfoParts('symbols');
      var ix = binarySearch(symObjs, symbol, (ix) => {
        return symObjs[ix]['symbol'];
      });
      if (ix > -1) {
        const s = symObjs[ix]['symbol'];
        if (!(_symbols[s])) {
          var symObj = { symbol:s }
          _symbols[s] = symObj;
          _symbols[_symbols.length] = s;
        }
      }
    }
  }

  function _getSymbols() {
    return _symbols;
  }

  function _addTickerPrice(tp) {
    if (tp && tp['symbol']) {
      var symObj = _symbols[tp['symbol']];
      if (symObj) {
        symObj['tp'] = tp;
      }
    }
  }

  function _getTickerPrices() {
    return _symbols.map(s => _symbols[s]);
  }

  return {
    setExchangeInfo: setExchangeInfo,
    getExchangeRateLimits: () => {
      return _getExchangeInfoParts('rateLimits');
    },
    getExchangeSymbols: () => {
      return _getExchangeInfoParts('symbols');
    },
    addSymbol: _addSymbol,
    getSymbols: _getSymbols,
    addTickerPrice: _addTickerPrice,
    getTickerPrices: _getTickerPrices
  };
}

// https://api.binance.com/api/v1/ticker/24hr?symbol=XRPBTC
// https://api.binance.com/api/v3/ticker/price?symbol=XRPBTC

/**
 * Contains the API calls to Binance exchange.
 * 
 * @param {*} cfg 
 */
function BinanceExchangeAPI(cfg) {
  if (!(this instanceof BinanceExchangeAPI)) {
    return new BinanceExchangeAPI();
  }
  cfg = (cfg)? cfg:{};
  const _apiServer = (cfg['apiServer'])? cfg['apiServer']:'https://api.binance.com';
  const _apiExchangeInfo = (cfg['apiExchangeInfo'])? cfg['apiExchangeInfo']:'/api/v1/exchangeInfo';
  const _apiTickerPrice = (cfg['apiTickerPrice'])? cfg['apiTickerPrice']:'/api/v3/ticker/price';

  function _getApi(url, dataCB, errorCB) {
    https.get(url, (resp) => {
      let data = '';
      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk;
      });
      // The whole response has been received.
      resp.on('end', () => {
        dataCB(JSON.parse(data));
      });
    }).on("error", (err) => {
      errorCB(err);
    });
  }

  return {
    loadExchangeInfo: (dataCB, errorCB) => {
      _getApi(_apiServer + _apiExchangeInfo, dataCB, errorCB);
    },
    loadTickerPrice: (symbol, dataCB, errorCB) => {
      _getApi(_apiServer + _apiTickerPrice + '?symbol=' + symbol, dataCB, errorCB);
    }
  };
}
