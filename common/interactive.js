/**
 * Interactive looper test module
 */
'use strict'

var rightTrimRE = /\s+$/;
var leftTrimRE = /^\s+/;

function stripNL(chunk, defVal) {
	var line = chunk.toString(); //('utf8');
	line = line.replace(rightTrimRE,'');
	if (line.length == 0 && defVal) {
		return defVal;
	}
	return line;
}

function getQuestioner() {
	var _questions = [];
	var _defaultQuestion = 'pls enter data';
	var _defaultValidate = function(val) {
		return val;
	};

	/**
	 * Clears all added questions.
	 */
	function clearQuestions() {
		_questions = [];
		return this;
	}

	function addQuestion(msg, defaultVal, validate) {
		msg = msg || _defaultQuestion;
		defaultVal = defaultVal || '';
		if (!(validate instanceof Function)) {
			validate = _defaultValidate;
		}
		var q = {
			msg: msg,
			defaultVal: defaultVal,
			validate: validate
		}
		_questions[_questions.length] = q;
		return this;
	}

	/**
	 * Asks added questions, recursively, until all questions have been answered,
	 * and then passes the answers to the ansCallback
	 * 
	 * @param ansCallback Function
	 * @param answers Array
	 */
	function askQuestion(ansCallback, answers) {
		if (ansCallback instanceof Function) {
			answers = answers || [];
			var elm = _questions.shift();	
			if (elm) {
				// get input
				process.stdout.write(elm.msg + ' ==>');
				process.stdin.once('data', function(chunk) {
					elm.val = stripNL(chunk, elm.defaultVal);
					answers.push(elm);
					// get input for the rest
					askQuestion(ansCallback, answers);
				});
			} else {
				ansCallback(answers);
			}
		}
	}

	return {
		addQuestion: addQuestion,
		/**
		 * Asks added questions, recursively, until all questions
		 * have been answered, and then passes the answers to the ansCallback.
		 * 
		 * @param ansCallback Function
		 */
		askQuestions: function(ansCallback){
			askQuestion(ansCallback);
		},
		/**
		 * Returns an answer from the 'answers' based on position 'i'
		 *  - starts at '0' for first question
		 *  
		 *  @param answers Array
		 *  @param i Number
		 */
		getAnswer: function(answers, i) {
			answers = answers || [];
			return answers[i]['val'];
		},
		clearQuestions: clearQuestions
	};
}

function getLooper() {
	var _opts = [];
	var _exit = processExit;
		
	function processExit() {
		console.log("bye");
		process.exit(0);
	}
	
	/**
	 * Clears any current options that are set.
	 * Options start at '1'
	 */
	function clearOptions() {
		_opts = [];
		_opts[0] = null;
		return this;
	}

	function processOption(chunk) {
		if (chunk.length > 1) {
			var opt = stripNL(chunk);
			if (!isNaN(opt)) {
				var ix = Number(opt);
				var _opt = _opts[ix];
				if (_opt && _opt.cb instanceof Function) {
					listen(false);
					_opt.cb();
					return;
				}
			} else if ('exit'.indexOf(opt) == 0) {
				listen(false);
				_exit();
				return;
			}
		}
		showOptions();
	}
	
	function listen(on) {
		if (on) {
			process.stdin.on('data', processOption);
			showOptions();
		} else {
			process.stdin.removeListener('data', processOption);
		}
	}
	
	function showOptions() {
		process.stdout.write('\nHere are the options:\n\n');
		_opts.forEach(function(opt, i) {
			if (i > 0) {
				process.stdout.write('  ' + i + ') ' + opt.optMsg + '\n');
			}
		});
		process.stdout.write('  exit) exit loop\n\n');
		process.stdout.write("pls enter an option ==>");
	}
	
	/**
	 * Adds option to the looper, optMsg and optCallback both mandatory.
	 * The optCallback MUST call listen().
	 * 
	 * @param optMsg string
	 * @param optCallback Function
	 */
	function addOption(optMsg, optCallback) {
		if (optCallback instanceof Function) {
			optMsg = optMsg || 'test option';
			_opts[_opts.length] = {
					optMsg : optMsg,
					cb : optCallback
			};
		}
		return this;
	}
	
	/**
	 * Set the loop exit function - use when you need inner loops that
	 * should call back to the parent loop - probably it's listen().
	 * TODO find a better way to do this - maybe parent loop?
	 * 
	 * @param exit Function
	 */
	function setExit(exit) {
		if (exit instanceof Function) {
			_exit = exit;
		} else {
			_exit = processExit;
		}
		return this;
	}
	
	clearOptions();
		
	return {
		clearOptions: clearOptions,
		addOption: addOption,
		/**
		 * Looper starts listening for an option to be selected.
		 * This must be called as the last function in optCallback.
		 */
		listen: function() {
			listen(true);
		},
		setExit: setExit
	};	
}

module.exports = {
		questioner: getQuestioner,
		looper: getLooper
};
