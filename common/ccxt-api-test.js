/**
 * Node file to test the ccxt API via a console app
 */
'use strict'

const ccxt = require("ccxt");
const interactive = require('./interactive');

const looper = interactive.looper();
const questioner = interactive.questioner();

function ExchangeMgr() {
    if (!(this instanceof ExchangeMgr)) {
        return new ExchangeMgr();
    }
    
    const exchangeLookup = [];

    function _addExchange(ccxtExchangeConstructor, create = false) {
        if (!(typeof ccxtExchangeConstructor === 'function')) {
            console.log('not function');
            return {};
        }
        var _f = exchangeLookup[ccxtExchangeConstructor['name']];
        if (_f) {
            console.log('already defined');
            return _f;
        }
        if (!create) {
            console.log('not created yet');
            return {};
        }
        var ccxtExchange = new ccxtExchangeConstructor();
        exchangeLookup[ccxtExchangeConstructor['name']] = ccxtExchange;
        exchangeLookup[exchangeLookup.length] = ccxtExchange;
        console.log('created');
        return ccxtExchange;
    }

    return {
        addExchange: (ccxtExchangeConstructor) => {
            return _addExchange(ccxtExchangeConstructor, true);
        },
        getExchange: (ccxtExchangeConstructor) => {
            return _addExchange(ccxtExchangeConstructor);
        },
        getExchanges: exchangeLookup
    };
}

const exchangeMgr = new ExchangeMgr();

looper.clearOptions()
.addOption('display option', function() {
	console.log('display crap');
	looper.listen();
})
.addOption('load binance exchange', function() {
    console.log('we are about to load binance exchange...');
    (async function () {
        var func = ccxt.binance;
        if ( typeof func === 'function') {
            console.log(func['name']);
            var exchg = new func();
            console.log(exchg.id);
        }
        //console.log(exchg.id, await exchg.loadMarkets());        
        looper.listen();
    }) ();
})
.addOption('create binance exchange via exchange manager', function() {
    console.log('we are about to create binance exchange...');
    var exchg = exchangeMgr.addExchange(ccxt.binance);
    console.log(exchg.id + ' is loaded');
    looper.listen();
})
.addOption('load binance markets', function() {
    console.log('we are about to load binance markets...');
    var exchg = exchangeMgr.getExchange(ccxt.binance);
    (async function() {
        var mkts = await exchg.loadMarkets();
        looper.listen();
    }) ();
})
.listen();