# TODO #

This todo is a simple requirements doc.
It should caputure progress and all requirements that we want.

### REFERENCES ###

* study the [binance API doc](https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md)
* study the existng binance-api-test.js
    * use the [Mozilla Javascript Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
    * use the [Node.JS API](https://nodejs.org/dist/latest-v8.x/docs/api/)


### REQUIREMENTS ###


### IN PROGRESS ###


### DEVELOPMENT & TESTING ###

* Understand the code and why we implement in a certain way
    * NODE runs in a event loop
    * NPM basic cmds to load and manage modules
* Use interactive tests to tease out code and logic.
* Move useful code to appropriate JS files - the structure of the app is still to be resolved

